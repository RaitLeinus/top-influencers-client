function generateTopMenu() {
    let logoutLink = "";
    if (!isEmpty(getUsername())) {
        logoutLink = `${getUsername()} | <a href="javascript:logoutUser()" style="color: #fff; font-weight: bold;">logi välja</a>`;
    }

    document.getElementById("topMenuContainer").innerHTML = `
        <div style="padding: 5px; color: white; font-style: italic; background:black;">
            <div class="row">
                <div class="col-6">
                    <strong>SOTSIAALNUGA!</strong>
                </div>
                <div class="col-6" style="text-align: right; font-style: normal;">
                    ${logoutLink}
                </div>
            </div>
        </div>
    `;
}

function showMainContainer() {
    document.getElementById("loginContainer").style.display = "none";
    document.getElementById("mainContainer").style.display = "block";
}

function showLoginContainer() {
    document.getElementById("loginContainer").style.display = "block";
    document.getElementById("mainContainer").style.display = "none";
}


//location.pathname.substring(1) == 'index.html';