
// Controller functions here...

let allInfluencers = [];

function loadInfluencers() {
    fetchInfluencers().then(
        function (influencers){
            allInfluencers = influencers;
            displayInfluencers(allInfluencers)
        });
}

function displayInfluencers(influencers){
    let influencerHtml = "";
    let number = 1;

    for(let i=0; i < influencers.length; i++){
        influencerHtml = influencerHtml + `
            <tr>
                <td>
                    ${number++}
                </td>
                <td>
                    ${influencers[i].username}
                </td>
                <td>
                    <img src="${influencers[i].image}" height="75">  
                </td>
                <td>
                    ${influencers[i].platform_name}  
                </td>
                <td>
                    ${influencers[i].followers_count}
                </td>
            </tr>
       `;
    }
    document.getElementById("displayInfluencers").innerHTML = influencerHtml;
}

function handleAllButtonClick(){

    fetchInfluencers().then(displayInfluencers);
}

function handleInstagramButtonClick(){

    fetchInstagram().then(displayInfluencers);
}

function handleYoutubeButtonClick(){

    fetchYoutube().then(displayInfluencers);
}

function handleSearchkeyChange() {
    let searchKey = document.getElementById("searchKey").value;
    let filteredInfluencers = searchInfluencers(searchKey);
    displayInfluencers(filteredInfluencers);
}

function addInfluencer(){
    let newLink = document.getElementById("addKey").value;
    AddInfluencerToDatabase(newLink);
}

function searchInfluencers(searchKeyword) {
    let resultingInfluencers = [];

    for (let i = 0; i < allInfluencers.length; i++) {
        if (allInfluencers[i].username.toLowerCase().search(searchKeyword.toLowerCase()) != -1) {
            resultingInfluencers.push(allInfluencers[i]);
        }
    }

    return resultingInfluencers;
}


// ADMIN functions

function loginUser() {
    let credentials = getCredentialsFromLoginContainer();
    if (validateCredentials(credentials)) {
        login(credentials).then(session => {
            storeAuthentication(session);
            generateTopMenu();
            loadInfluencers();
        })
    }
}

function logoutUser() {
    clearAuthentication();
    generateTopMenu();
    showLoginContainer();
}



