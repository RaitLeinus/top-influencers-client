
// Functions for communicating with backend-API here...


function fetchInfluencers() {
    return fetch(
        `${API_URL}/influencers`,
        {
            method: 'GET'
        }
    )
    .then(response => response.json());
}

function fetchInstagram() {
    return fetch(
        `${API_URL}/influencer?platform_name=Instagram`,
        {
            method: 'GET'
        }
    )
    .then(something => something.json());
}

function fetchYoutube() {
    return fetch(
        `${API_URL}/influencer?platform_name=Youtube`,
        {
            method: 'GET'
        }
    )
    .then(something => something.json());
}

function fetchByUsername(username){
    return fetch(
        `${API_URL}/influencerusername?username=${username}`,
        {
            method: 'GET'
        }
    )
    .then(something => something.json()); 
}

function AddInfluencerToDatabase(link){

    let formData = new FormData();
    formData.append("user_link", link);

    return fetch(
            `${API_URL}/add`, 
            {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${getToken()}`
                },
                body: formData
            }
    )
    .then(checkResponse);
}

//ADMIN functions

function login(credentials) {
    return fetch(
        `${API_URL}/users/login`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(credentials)
        }
    )
    .then(checkResponse)
    .then(session => session.json());
}

function checkResponse(response) {
    if (!response.ok) {
        clearAuthentication();
        showLoginContainer();   
        generateTopMenu();
        throw new Error(response.status);
    }
    
    showMainContainer();
    return response;
}


